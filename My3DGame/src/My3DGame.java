import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;


public class My3DGame {
	private static int width;
	private static int height;
	private static boolean fullscreen;
	private static final String TITLE = "My 3D Game";


	public My3DGame(int width, int height, boolean fullscreen) {
		this.fullscreen = fullscreen;
		this.width = width;
		this.height = height;
		
		
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			Display.setTitle(TITLE);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			Display.destroy();
			System.exit(1);
		}
		
		while (!Display.isCloseRequested()) {
			Display.update();
			Display.sync(60);
			
		}
		
		Display.destroy();
		System.exit(0);
	}
	
	public static void main(String[] args) {
		new My3DGame(800, 600, false);
	}
}
