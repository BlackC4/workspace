package de.wendt.survive;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

public class Field {

	private int Size = 128;
	private int x;
	private int y;

	public Field(int x, int y, int scale) {
		this.x = x;
		this.y = y;
		this.Size = 128 >> scale;
	}

	public void renderField(float xMove, float yMove) {
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f((x * Size) + xMove, y * Size + yMove); // Upper-left
		glTexCoord2f(1, 0);
		glVertex2f((x * Size) + Size + xMove, y * Size + yMove); // Upper-right
		glTexCoord2f(1, 1);
		glVertex2f((x * Size) + Size + xMove, (y * Size) + Size + yMove); // Bottom-right
		glTexCoord2f(0, 1);
		glVertex2f(x * Size + xMove, (y * Size) + Size + yMove); // Bottom-left
		glEnd();
	}
}
