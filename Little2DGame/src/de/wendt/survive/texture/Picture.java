package de.wendt.survive.texture;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Picture {

	private static Texture pic;

	public Picture(String name) {
		try {
			// Load the wood texture from "res/images/wood.png"
			pic = TextureLoader.getTexture("PNG", new FileInputStream(new File("res/texture/" + name + ".png")));
		} catch (IOException e) {
			e.printStackTrace();
			Display.destroy();
			System.exit(1);
		}
	}
	
	public Texture getTexture(){
		return pic;
	}
}
