package de.wendt.survive;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glVertex2f;

import org.newdawn.slick.opengl.Texture;

import de.wendt.survive.texture.Picture;

public class Player {

	private int Size = 128;
	private int x;
	private int y;

	Texture playerTex[] = new Texture[1];
//	Picture pic = new Picture();

	public Player(int x, int y) {
		this.x = x;
		this.y = y;
		
	}

	public void renderPlayer() {
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f((x * Size), y * Size); // Upper-left
		glTexCoord2f(1, 0);
		glVertex2f((x * Size) + Size, y * Size); // Upper-right
		glTexCoord2f(1, 1);
		glVertex2f((x * Size) + Size, (y * Size) + Size); // Bottom-right
		glTexCoord2f(0, 1);
		glVertex2f(x * Size, (y * Size) + Size); // Bottom-left
		glEnd();
	}
	
	public Texture getTexture(){
		return playerTex[0];
	}
}
