package de.wendt.survive;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;

public class KeyInput {

	private static int x;
	private static int y;
	private static int shift;

	public KeyInput() {
		try {
			Keyboard.create();
		} catch (LWJGLException e) {

			e.printStackTrace();
		}
	}

	public void KeyUpdate() {
		// bewegung
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)||Keyboard.isKeyDown(Keyboard.KEY_A)) {
			x+= 2 + shift;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)|| Keyboard.isKeyDown(Keyboard.KEY_D)) {
			x-= 2 + shift;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_UP) || Keyboard.isKeyDown(Keyboard.KEY_W)) {
			y+= 2 + shift;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN) || Keyboard.isKeyDown(Keyboard.KEY_S)) {
			y-= 2 + shift;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			shift = 1;
		}
		if (!Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
			shift = 0;
		}
		
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

}
